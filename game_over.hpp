#pragma once

#include "gui.hpp"
#include "config.hpp"

#include <iostream>

class guiGameOver : public gui {
public:
	guiGameOver() : 
		gui()
	{
		m_title.setFont(config::fonts::def);
		m_score.setFont(config::fonts::def);
		m_wave.setFont(config::fonts::def);
		m_info.setFont(config::fonts::def);

		m_title.setCharacterSize(32u);
		m_title.setString("game-over");

		m_info.setCharacterSize(16u);
		m_info.setString("press any key");

		m_score.setCharacterSize(16);
		m_wave.setCharacterSize(16);

		m_title.setPosition(sf::Vector2f(0, 50));
		m_score.setPosition(sf::Vector2f(0, 100));
		m_wave.setPosition(sf::Vector2f(0, 120));
		m_info.setPosition(sf::Vector2f(0, 140));
	}

	void update(unsigned int score, unsigned int wave) {
		m_score.setString("your score " + std::to_string(score));
		m_wave.setString("you died at " + std::to_string(wave) + " wave");
	}

	void draw(sf::RenderWindow* window) override {
		window->draw(m_title);
		window->draw(m_score);
		window->draw(m_wave);
		window->draw(m_info);
	}

private:
	sf::Text m_title;
	sf::Text m_score;
	sf::Text m_wave;
	sf::Text m_info;

};