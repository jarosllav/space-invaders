#include "game.hpp"

int main() {
	config::WindowSize = sf::Vector2i(800, 600);
	if (config::fonts::def.loadFromFile("data/fonts/default.ttf"))
		std::cout << "Loaded default font into memory\n";

	game app(sf::VideoMode(config::WindowSize.x, config::WindowSize.y), "space-invaders", 120u);
	app.run();

	return EXIT_SUCCESS;
}