#pragma once

#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include "config.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "wave.hpp"

#include "gui.hpp"
#include "game_over.hpp"

class game final {
public:
	game(const sf::VideoMode& videoMode, const std::string& title, unsigned int fps) :
		m_window(std::make_unique<sf::RenderWindow>(videoMode, title)),
		m_player({350.f, 500.f}, {25.f, 25.f}),
		m_timePerFrame(sf::seconds(1.f / fps))
	{
		//m_window->setFramerateLimit(120u);
		m_window->setVerticalSyncEnabled(false);
		m_window->setMouseCursorVisible(false);

		setup();
	}

	void setup() {
		//m_waves.push_back(wave("data/test_wave3.txt"));
		m_waves.push_back(wave("data/test_wave.txt"));
		m_waves.push_back(wave("data/test_wave2.txt"));

		m_currentWave = 0;
	}

	void run() {
		while (m_window->isOpen()) {
			pollEvent();
			sf::Time elapsedTime = m_clock.restart();
			m_time += elapsedTime;
			while (m_time > m_timePerFrame) {
				m_time -= m_timePerFrame;
				if (m_windowFocus && !m_gameOverScreen.isOpen())
					update();
			}
			draw();
		}
	}

private:
	void pollEvent() {
		while (m_window->pollEvent(m_event)) {
			if (m_event.type == sf::Event::Closed)
				m_window->close();
			else if (m_event.type == sf::Event::KeyPressed || m_event.type == sf::Event::MouseButtonPressed && m_gameOverScreen.isOpen()) {
				m_gameOverScreen.close();
				m_window->setMouseCursorVisible(false);
				m_currentWave = 0;
				m_waves[m_currentWave].reset();
				m_score = 0;

				sf::Vector2f size = m_player.getSize();
				sf::Mouse::setPosition(sf::Vector2i(config::WindowSize.x / 2.f - size.x / 2.f, config::WindowSize.y - size.y), *m_window.get());
			}
			else {
				if (m_event.type == sf::Event::MouseButtonPressed) {
					if (m_windowFocus && sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
						sf::Vector2i position = sf::Mouse::getPosition(*m_window.get());
						position.x += m_player.getSize().x / 2.f;
						m_player.shoot(sf::Vector2f(position));
					}
				}
				else if (m_event.type == sf::Event::GainedFocus)
					m_windowFocus = true;
				else if (m_event.type == sf::Event::LostFocus)
					m_windowFocus = false;
			}
		}
	}

	void update() {
		m_player.update(m_window.get());

		auto& enemies = m_waves[m_currentWave].getEnemies();
		for (unsigned int y = 0; y < enemies.size(); ++y) {
			for (unsigned int x = 0; x < enemies[y].size(); ++x) {
				enemies[y][x].setPosition(enemies[y][x].getPosition() + sf::Vector2f(0.f, 0.025f));
			}
		}

		sf::Vector2f position = m_player.getPosition();
		sf::Vector2f size = m_player.getSize();
		for (auto& vec : enemies) {
			for (auto& enemy : vec) {
				sf::Vector2f enemyPosition = enemy.getPosition();
				sf::Vector2f enemySize = enemy.getSize();
				if (position.x + size.x >= enemyPosition.x && position.y + size.y >= enemyPosition.y &&
					position.x <= enemyPosition.x + enemySize.x && position.y <= enemyPosition.y + enemySize.y) {
					m_gameOverScreen.update(m_score, m_currentWave + 1);
					m_gameOverScreen.open();
					m_window->setMouseCursorVisible(true);
				}
			}
		}

		auto& bullets = m_player.getBullets();
		float bulletSize = m_player.getBulletSize();
		for (auto& bullet : bullets) {
			for (auto i = 0u; i < enemies.size(); ++i) {
				for(auto j = 0u; j < enemies[i].size(); ++j) {
					sf::Vector2f enemyPosition = enemies[i][j].getPosition();
					sf::Vector2f enemySize = enemies[i][j].getSize();
					if (bullet.x + bulletSize >= enemyPosition.x && bullet.y + bulletSize >= enemyPosition.y &&
						bullet.x <= enemyPosition.x + enemySize.x && bullet.y <= enemyPosition.y + enemySize.y) {
						bullets.erase(std::remove(bullets.begin(), bullets.end(), bullet), bullets.end());
						m_score += 1;

						m_waves[m_currentWave].destroy(j, i);
						if (m_waves[m_currentWave].getEnemiesCount() <= 0) {
							m_waves[m_currentWave].reset();
							++m_currentWave;
							if (m_currentWave >= m_waves.size()) {
								m_currentWave = 0; //todo game over
								m_waves[m_currentWave].reset();
							}
						}
					}
				}
			}
		}
	}

	void draw() {
		m_window->clear(sf::Color::Black);
		m_player.draw(m_window.get());
		for (auto& enemy : m_enemies)
			enemy.draw(m_window.get());

		m_waves[m_currentWave].draw(m_window.get());

		if (m_gameOverScreen.isOpen())
			m_gameOverScreen.draw(m_window.get());

		m_window->display();
	}

	bool m_windowFocus = true;
	std::unique_ptr<sf::RenderWindow> m_window;
	sf::Event m_event;
	sf::Clock m_clock;
	sf::Time m_time = sf::Time::Zero;
	sf::Time m_timePerFrame;

	player m_player;
	std::vector<enemy> m_enemies;
	std::vector<wave> m_waves;
	unsigned int m_currentWave = 0;
	unsigned int m_score = 0;

	guiGameOver m_gameOverScreen;
};