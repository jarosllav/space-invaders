#pragma once

#include <fstream>
#include <vector>

#include "enemy.hpp"

class wave final {
public:
	wave(const std::string& fileName) {
		/*
			wave.txt
			[width height]
			[enemy size]
			[...] // width * height segment 0-1	
		*/
		std::ifstream file(fileName);
		if (file.is_open()) {
			unsigned int width = 0, height = 0;
			
			file >> width >> height;
			if (width > 0 && height > 0) {
				unsigned int enemySize = 32u;
				file >> enemySize;

				int xOffset = (config::WindowSize.x / width) / 2.f;

				m_enemies.resize(height);
				for (auto i = 0u; i < height; ++i) {
					unsigned int isEnemy = 0;
					for (auto j = 0u; j < width; ++j) {
						file >> isEnemy;
						if (isEnemy == 1) {
							m_enemies[i].push_back(enemy(sf::Vector2f(xOffset + j * (enemySize + xOffset), i * enemySize), sf::Vector2f(enemySize, enemySize)));
						}
					}
				}
			}
			m_baseEnemies = m_enemies;

			file.close();
		}
	}

	void reset() {
		m_enemies = m_baseEnemies;
	}

	void destroy(unsigned int x, unsigned int y) {
		m_enemies[y].erase(m_enemies[y].begin() + x);
		if (m_enemies[y].size() <= 0)
			m_enemies.erase(m_enemies.begin() + y);
	}

	void draw(sf::RenderWindow* window) {
		for (auto& row : m_enemies) {
			for (auto& enemy : row) {
				enemy.draw(window);
			}
		}
	}

	inline std::vector<std::vector<enemy>>& getEnemies() { return m_enemies; }
	inline unsigned int getEnemiesCount() const { 
		//todo
		return m_enemies.size();
	}

private:
	std::vector<std::vector<enemy>> m_enemies;
	std::vector<std::vector<enemy>> m_baseEnemies;
};