#pragma once

#include "object.hpp"

class enemy final : public object {
public:
	enemy(const sf::Vector2f& position, const sf::Vector2f& size) :
		object(position, size)
	{
		m_shape.setPosition(position);
		m_shape.setSize(size);
	}

	void draw(sf::RenderWindow* window) override {
		window->draw(m_shape);
	}

	void update() override {

	}

	inline void setPosition(const sf::Vector2f& position) { m_position = position; m_shape.setPosition(position); }

private:
	sf::RectangleShape m_shape;
};