#pragma once

#include <SFML/Graphics.hpp>

namespace config {
	sf::Vector2i WindowSize;

	namespace fonts {
		sf::Font def;
	}
}