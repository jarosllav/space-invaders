#pragma once

#include <SFML/Graphics.hpp>

class gui {
public:
	gui() :
		m_isOpen(false)
	{

	}

	virtual void open() { m_isOpen = true; }
	virtual void close() { m_isOpen = false; }

	virtual void draw(sf::RenderWindow* window) {}

	inline bool isOpen() const { return m_isOpen; }
protected:
	bool m_isOpen;
};