#pragma once

#include <vector>

#include "config.hpp"
#include "object.hpp"

class player final : public object {
public:
	player(const sf::Vector2f& position, const sf::Vector2f& size) :
		object(position, size)
	{
		m_shape.setPosition(position);
		m_shape.setSize(size);
		m_shape.setFillColor(sf::Color::Blue);

		m_bullet.setSize(sf::Vector2f(m_bulletSize, m_bulletSize));
		m_bullet.setFillColor(sf::Color::Red);
	}

	void draw(sf::RenderWindow* window) override {
		for (auto& bullet : m_bullets) {
			m_bullet.setPosition(bullet);
			window->draw(m_bullet);
		}
		window->draw(m_shape);
	}

	void update(sf::RenderWindow* window) {
		sf::Vector2f position;
		position.x = std::clamp(sf::Mouse::getPosition(*window).x, 0, config::WindowSize.x - static_cast<int>(m_size.x));
		position.y = std::clamp(sf::Mouse::getPosition(*window).y, 0, config::WindowSize.y - static_cast<int>(m_size.y));

		m_shape.setPosition(position);

		for (auto& bullet : m_bullets) {
			bullet.y -= m_bulletSpeed;
			if (bullet.y <= 0) {
				m_bullets.erase(std::remove(m_bullets.begin(), m_bullets.end(), bullet), m_bullets.end());
			}
		}
	}

	void shoot(const sf::Vector2f& position) {
		m_bullets.push_back(position);
	}

	inline void setBulletTime(float time) { m_bulletSpeed = time; }

	inline float getBulletTime() const { return m_bulletSpeed; }
	inline float getBulletSize() const { return m_bulletSize; }
	inline std::vector<sf::Vector2f>& getBullets() { return m_bullets; }
	inline sf::RectangleShape getShape() const { return m_shape; }
	inline sf::Vector2f getPosition() const { return m_shape.getPosition(); }
	inline sf::Vector2f getSize() const { return m_shape.getSize(); }

private:
	float m_bulletSpeed = 7.5f;
	float m_bulletSize = 5.f;
	sf::RectangleShape m_shape;
	sf::RectangleShape m_bullet;
	std::vector<sf::Vector2f> m_bullets;
};