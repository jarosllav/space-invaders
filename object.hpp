#pragma once

#include <algorithm>

#include <SFML/Graphics.hpp>

class object {
public:
	object(const sf::Vector2f& position, const sf::Vector2f& size) :
		m_position(position),
		m_size(size)
	{

	}

	inline void setPosition(const sf::Vector2f& position) { m_position = position; }
	inline void setSize(const sf::Vector2f& size) { m_size = size; }

	inline sf::Vector2f getPosition() const { return m_position; }
	inline sf::Vector2f getSize() const { return m_size; }

protected:
	virtual void draw(sf::RenderWindow* window) {};
	virtual void update() {};

	sf::Vector2f m_position;
	sf::Vector2f m_size;
};